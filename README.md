[![](https://gitlab.cern.ch/allpix-squared/allpix-squared/raw/master/doc/logo_small.png)](https://cern.ch/allpix-squared)

# CMake Structure for Building External Allpix<sup>2</sup> Modules

This repository contains CMake scaffolding for building external modules for Allpix<sup>2</sup> by linking against the framework's core libraries.
For more details about the Allpix<sup>2</sup> project please have a look at the website at https://cern.ch/allpix-squared.

## Documentation
The usage of this build system is described in the user manual of the Allpix<sup>2</sup> framework.
The most recently published version of the User Manual is available online [here](https://cern.ch/allpix-squared/usermanual/allpix-manual.pdf).

## Contributing
All types of contributions, being it minor and major, are very welcome. Please refer to our [contribution guidelines](https://gitlab.cern.ch/allpix-squared/allpix-squared/tree/master/CONTRIBUTING.md) for a description on how to get started.

Before adding changes it is very much recommended to carefully read through the documentation in the User Manual first.

## Licenses
This software is distributed under the terms of the MIT license. A copy of this license can be found in [LICENSE.md](LICENSE.md).

The documentation is distributed under the terms of the CC-BY-4.0 license. This license can be found in [doc/COPYING.md](doc/COPYING.md).
